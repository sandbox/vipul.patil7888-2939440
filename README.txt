
-- SUMMARY --

In a webform module in Drupal there is a provision for redirecting Webform submission to either inbuilt confirmation page, custom URL OR
no redirection. Then in case of no redirection user when submits webform doesn't get notifications of submission by default. 
User either have to alter webform and set the drupal message on custom submit OR in some cases if user wishes to have a pop-up on submit 
then in that case developer have to do a lot of R&D for pop-up and write a long custom code. 
So to overcome all these overhead "Webform Confirmation Pop-up" module is introduced.

For a full description of the module, visit the project page:
  http://drupal.org/project/webform_confirmation_pop_up

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/webform_confirmation_pop_up


-- REQUIREMENTS --
Install Webform Module as a dependency.





